autoscale: true

#[fit]Day 3 Session 2

## Convolutional Deep Learning

---

## Why does deep learning work?

1. Automatic differentiation 
2. GPU
3. Learning Representations

MLP's constructed "recursive functions":

$$s(w_n.z_n + b_n)$$ where $$z_n = s(w_{n-1}.z_{n-1}+b_{n-1})$$ and so on.

---

## Convolutional Networks

- pay attention to the spatial locality of images
- this is done through the use of "filters"
- thus the representations learnt are spatial and bear a mapping to reality
- and are hierarchical..later layers learn features composed from the previous layers
- perhaps even approximating what the visual cortex does.

---

##[fit] Deep Learning Components

---

## Fully Connected layers

- also called dense layers
- these are the layers in MLPs
- we typically use them after convolutional layers to feed to a softmax to get probabilities

![right, fit](/Users/rahul/Projects/DeepLearningBookFigures-Volume2/Chapter20-DeepLearning/Figure-20-006.png)

---

## 1-1 layers

- nonlinearities and softmax
- we dont really consider them layers ..you can think of them bolted to previous nodes in each layer.
- they are nor fully-connected..indeed theay are 1-1

---

## Dropout

- not really a layer, but a technique
- randomly sever connections to and from some fraction of nodes in "previous" layer
- a regularization method, it prevents overfitting by not allowing specific nodes to specialize to say, for example "cat eye detection" at the expense of other things. By severing, other neurons are forced to step in
- only during training, at prediction time the connections are restored

![left, fit](/Users/rahul/Projects/DeepLearningBookFigures-Volume2/Chapter20-DeepLearning/Figure-20-008.png)

---

## Batch Normalization

- also not a layer, but does do some computation
- like input normalization, we standardize the outputs from a layer, usually a fully connected layer
- but we do it before going to the non-linearity, because the whole idea is to move the outputs to a regime where the non-linear activation has some action (is not saturated)

![right, fit](/Users/rahul/Projects/DeepLearningBookFigures-Volume2/Chapter20-DeepLearning/Figure-20-009.png)

---

## Convolution Layer

- applies a convolution kernel to an image
- the kernel may have depth, and can use padding on the original image and strides in deciding how to move the kernel window over the image
- the output image(s) depend on these paddings, strides, size of kernel, depth of kernel (channels)
- the kernel usually has weights which multiply the local 'x' in the image and perhaps add bias..in other words, a linear convolution

![right, fit](/Users/rahul/Projects/DeepLearningBookFigures-Volume2/Chapter20-DeepLearning/Figure-20-010.png)

---

## Pooling Layer

- down samples images, usually using a non-overlapping stride
- 2 common types, max-pooling and average-pooling
- makes images smaller for manageability, and also helps in learning at the next level in the hierarchy

![left, fit](/Users/rahul/Projects/DeepLearningBookFigures-Volume2/Chapter20-DeepLearning/Figure-20-011.png)

---

## Other layers we wont talk about now

- recurrent (repeats an op, has a notion of memory)
- normalization, noise, reshaping, cropping
- zero-padding (used in conjunction with convolutionals)
- flattening before feeding to a fully connected, for example
- upsampling by repeating elements
- downsampling using the convolutions instead of pooling

---

## Layer types schematic

![inline](/Users/rahul/Projects/DeepLearningBookFigures-Volume2/Chapter20-DeepLearning/Figure-20-013.png)

---

## VGG16 Image Classification Example

![inline](/Users/rahul/Projects/DeepLearningBookFigures-Volume2/Chapter20-DeepLearning/Figure-20-017.png)

---

## The idea of a filter: detecting yellow

![inline](/Users/rahul/Projects/DeepLearningBookFigures-Volume2/Chapter21-CNNs/Figure-21-003.png)![inline](/Users/rahul/Projects/DeepLearningBookFigures-Volume2/Chapter21-CNNs/Figure-21-005.png)

---

![inline](/Users/rahul/Projects/DeepLearningBookFigures-Volume2/Chapter21-CNNs/Figure-21-006.png)

---

## The local receptive field

- you almost always use a square filter, and place the answer at the center of the square
- thus make it odd-sized
- the pixel in the center on the filter is called the anchor,  and the pixel on the image below the anchor, the focus pixel
- here we'll take the 9 pizel values, multiply by 9 weights from the filter (itemwise) and add, and put that value in the focus pixel in a new image

![right, fit](/Users/rahul/Projects/DeepLearningBookFigures-Volume2/Chapter21-CNNs/Figure-21-008.png)

---

## Convolution looks for patterns

Movethe filter over the original image and produce a new one

![inline](/Users/rahul/Projects/DeepLearningBookFigures-Volume2/Chapter21-CNNs/Figure-21-014.png)
![inline](/Users/rahul/Projects/DeepLearningBookFigures-Volume2/Chapter21-CNNs/Figure-21-015.png)

![left, fit](/Users/rahul/Projects/DeepLearningBookFigures-Volume2/Chapter21-CNNs/Figure-21-009.png)

---

## Hierarchical Filters

- do we then need to know every pattern we can find? NO! We learn the weights.
- now we do this hierarchically, with each filter at the next layer
- we hope to learn representations made up from smaller scale representations and so on
- here is an example: find the LHS face in the RHS image...

![right, fit](/Users/rahul/Projects/DeepLearningBookFigures-Volume2/Chapter21-CNNs/Figure-21-018.png)

---

## Strategy

![left, fit](/Users/rahul/Projects/DeepLearningBookFigures-Volume2/Chapter21-CNNs/Figure-21-019.png)

- Move layer 1 filters around
- max pool 27x27 to 9x9
- x means dont care about value
- now apply second level filter to 9x9 image
- max pool again to 3x3 image
- apply level 3 filters and see if we activate

---

## Level 1

![inline](/Users/rahul/Projects/DeepLearningBookFigures-Volume2/Chapter21-CNNs/Figure-21-020.png)

---

![inline](/Users/rahul/Projects/DeepLearningBookFigures-Volume2/Chapter21-CNNs/Figure-21-021.png)

---

## Level 2

![inline](/Users/rahul/Projects/DeepLearningBookFigures-Volume2/Chapter21-CNNs/Figure-21-022.png)

---

## Level 3

- here we designed our filter by  hand
- in real life we'd have users mark up the images they found by hand, and then learn these filters
- we'd be learning a hierarchical set of filters, or a hierarchical representation
- this sort of match requires polling (is there any signal?), but one can also downsample to create hierarchies by striding

![left, fit](/Users/rahul/Projects/DeepLearningBookFigures-Volume2/Chapter21-CNNs/Figure-21-023.png)

---

## Multiple Channel Use Cases

![inline](/Users/rahul/Projects/DeepLearningBookFigures-Volume2/Chapter21-CNNs/Figure-21-034.png)![inline](/Users/rahul/Projects/DeepLearningBookFigures-Volume2/Chapter21-CNNs/Figure-21-035.png)

---

![inline](/Users/rahul/Projects/DeepLearningBookFigures-Volume2/Chapter21-CNNs/Figure-21-036.png)![inline](/Users/rahul/Projects/DeepLearningBookFigures-Volume2/Chapter21-CNNs/Figure-21-037.png)

---

## MNIST architecture from Keras Docs

![inline](/Users/rahul/Projects/DeepLearningBookFigures-Volume2/Chapter21-CNNs/Figure-21-048.png)

---


---

```python
batch_size = 128
num_classes = 10
epochs = 12

# input image dimensions
img_rows, img_cols = 28, 28


model = Sequential()
model.add(Conv2D(32, kernel_size=(3, 3),
                 activation='relu',
                 input_shape=input_shape))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(num_classes, activation='softmax'))

model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.Adadelta(),
              metrics=['accuracy'])
```